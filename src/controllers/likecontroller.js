import { errorCode, failCode, successCode } from "../configs/response.js";
import initModels from "../models/init-models.js"
import sequelize from "../models/model.js";

let model = initModels(sequelize);

let createLikeRes = async (req,res)=>{
try {
let {user_id,res_id,date_like} = req.body;
let newLike = {user_id,res_id,date_like};
if( await model.like_res.findOne({where:{user_id,res_id} }) === null){
    let data = await model.like_res.create(newLike);
successCode(res,data,"Tạo dữ liệu thành công !")
}else{
    failCode(res,null,"lỗi FrontEnd")
}
} catch {
    errorCode(res,"Lỗi BackEnd!")
}
};


let unLikeRres = async (req,res)=>{
try {
    let {user_id,res_id} = req.params;
if( await model.like_res.findOne({where:{user_id,res_id}})){
    let data = await model.like_res.destroy({where:{user_id,res_id}})
    successCode(res,data,"Xóa dữ liệu thành công !")
}else{
    failCode(res,null,"lỗi FrontEnd")
}
} catch  {
    errorCode(res,"Lỗi Backenđ")
}
};

let  getListLikeRes = async (req,res)=>{
    try {
        let {res_id}=req.params;
if(await model.restaurant.findOne({include:["like_res"],where:{res_id}})){
    let data = await model.restaurant.findOne({include:["like_res"],where:{res_id}})
    successCode(res,data,"Lấy dữ liệu thành công !") 
}else{
    failCode(res,null,"lỗi FrontEnd")
}
    } catch  {
        errorCode(res,"Lỗi BackEnd")
    }
};
let  getListLikeUser = async (req,res)=>{
    try {
        let {user_id}=req.params;
  if(await model.user.findOne({include:["like_res"],where:{user_id}})){
    let data = await model.user.findOne({
        include:["like_res"],where:{user_id}
    })
    successCode(res,data,"Lấy dữ liệu thành công!")
  }else{
    failCode(res,null,"lỗi FrontEnd")
  }
    } catch  {
        errorCode(res,"Lỗi BackEnd")
    }
}




export {createLikeRes,unLikeRres,getListLikeRes,getListLikeUser}


