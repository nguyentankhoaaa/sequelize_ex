import { errorCode, failCode, successCode } from "../configs/response.js";
import initModels from "../models/init-models.js"
import sequelize from "../models/model.js";

let model = initModels(sequelize);

let createOrder = async (req,res)=>{
    try {
        let {user_id,food_id,amount,code,arr_sub_id}=req.body;
        let newOrder = {user_id,food_id,amount,code,arr_sub_id};
        let data = await model.orders.create(newOrder);
        successCode(res,data,"Tạo dữ liệu thành công")
    } catch {
errorCode(res,"Lỗi BackEnd")
    }
}
export {createOrder}