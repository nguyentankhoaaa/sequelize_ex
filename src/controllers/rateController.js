import { errorCode, failCode, successCode } from "../configs/response.js";
import initModels from "../models/init-models.js";
import sequelize from "../models/model.js";

let model = initModels(sequelize);



let createRate = async (req,res)=>{

try {
    let {user_id,res_id,amount,date_rate} = req.body;
    let newRate = {user_id,res_id,amount,date_rate};
    if(  await model.rate_res.findOne({where:{user_id,res_id}}) === null){
    let data =  await model.rate_res.create(newRate);
    successCode(res,data,"Lấy dữ liệu thành công");
    }else{
    failCode(res,null,"Lỗi FrontEnd")
    }  
} catch  {
    
    errorCode(res,"Lỗi BackEnd")
}
};


let getListRateRes = async (req,res)=>{
    try {
        let {res_id } = req.params;
     if(await model.restaurant.findOne({include:["rate_res"],where:{res_id}})){
        let data = await model.restaurant.findOne({include:["rate_res"],where:{res_id}})
        successCode(res,data,"Lấy dữ liệu thành công")
     }else{
        failCode(res,null,"Lỗi FrontEnd")
     }
    } catch  {
        errorCode(res,"Lỗi BackEnd")
    }
};


let getListRateUser = async(req,res)=>{
    try {
        let {user_id} = req.params;
        if( await model.user.findOne({include:["rate_res"],where:{user_id}})){
        let data =  await model.user.findOne({include:["rate_res"],where:{user_id}});
        successCode(res,data,"Lấy dữ liệu thành công")
        }else{
      failCode(res,null,"Lỗi FrontEnd")
        }
    } catch  {
        errorCode(res,"Lỗi BackEnd")
    }
}




export {createRate,getListRateRes,getListRateUser}