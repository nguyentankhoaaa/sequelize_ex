let successCode = (res,data,comment)=>{
    res.status(200).json({
        comment,
        content:data
    })
};


let failCode = (res,data,comment)=>{
    res.status(400).json({
        comment,
        content:data
    }) 
};
let errorCode = (res,comment)=>{
    res.status(500).json({
        comment,
    }) 
};

export {successCode,failCode,errorCode};