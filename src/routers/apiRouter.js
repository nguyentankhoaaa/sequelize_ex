import express from "express";
import { createLikeRes, getListLikeRes, getListLikeUser, unLikeRres } from "../controllers/likecontroller.js";
import { createRate, getListRateRes, getListRateUser } from "../controllers/rateController.js";
import { createOrder } from "../controllers/orderController.js";

let likeRoute = express.Router();
let rateRoute = express.Router();
let orderRoute = express.Router();

// like
likeRoute.post("/create-like",createLikeRes);
likeRoute.delete("/un-like/:user_id/:res_id",unLikeRres);
likeRoute.get("/get-list-like/res/:res_id",getListLikeRes);
likeRoute.get("/get-list-like/user/:user_id",getListLikeUser);

// rate

rateRoute.post("/create-rate",createRate);
rateRoute.get("/get-list-rate/res/:res_id",getListRateRes)
rateRoute.get("/get-list-rate/user/:user_id",getListRateUser)

// order
orderRoute.post("/create-order",createOrder);

export {likeRoute,rateRoute,orderRoute}