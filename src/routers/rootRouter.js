import express from "express";
import { likeRoute, orderRoute, rateRoute } from "./apiRouter.js";
let rootRouter = express.Router();

rootRouter.use("/like",likeRoute);
rootRouter.use("/rate",rateRoute);
rootRouter.use("/order",orderRoute);
export default rootRouter